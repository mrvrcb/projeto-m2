FROM azul/zulu-openjdk-alpine:8

VOLUME /tmp
ADD /target/mensal-2-1.0.0-SNAPSHOT.jar mensal-2.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/mensal-2.jar"]