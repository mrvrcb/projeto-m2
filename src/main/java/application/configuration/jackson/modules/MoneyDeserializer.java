package application.configuration.jackson.modules;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.io.IOException;

public class MoneyDeserializer extends StdDeserializer<Money> {

    public static final long serialVersionUID = 1L;

    public MoneyDeserializer() {
        this(null);
    }

    public MoneyDeserializer(Class<?> clazz) {
        super(clazz);
    }

    @Override
    public Money deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = p.getCodec().readTree(p);

        String currencyCode = node.get("currency").asText();
        Double value = node.get("value").asDouble();

        return Money.of(CurrencyUnit.of(currencyCode), value);
    }
}