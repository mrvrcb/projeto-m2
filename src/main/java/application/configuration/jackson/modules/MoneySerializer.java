package application.configuration.jackson.modules;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.joda.money.Money;

import java.io.IOException;

public class MoneySerializer extends StdSerializer<Money> {

    public static final long serialVersionUID = 1L;

    public MoneySerializer() {
        this(null);
    }

    public MoneySerializer(Class<Money> t) {
        super(t);
    }

    @Override
    public void serialize(Money value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeStartObject();
        jgen.writeStringField("currency", value.getCurrencyUnit().getCurrencyCode());
        jgen.writeNumberField("value", value.getAmount());
        jgen.writeEndObject();
    }
}

