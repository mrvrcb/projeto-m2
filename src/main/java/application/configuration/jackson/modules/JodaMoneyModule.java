package application.configuration.jackson.modules;

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.money.Money;

public class JodaMoneyModule extends SimpleModule {

    public static final long serialVersionUID = 1L;

    public JodaMoneyModule() {
        addSerializer(Money.class, new MoneySerializer());
        addDeserializer(Money.class, new MoneyDeserializer());
    }
}
