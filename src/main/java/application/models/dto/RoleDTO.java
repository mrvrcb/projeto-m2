package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@JsonSerialize(as = ImmutableRoleDTO.class)
@JsonDeserialize(as = ImmutableRoleDTO.class)
public interface RoleDTO {

    String name();

    List<String> authorities();
}
