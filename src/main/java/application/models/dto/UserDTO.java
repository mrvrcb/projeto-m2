package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.List;
import java.util.Optional;

@Value.Immutable
@JsonSerialize(as = ImmutableUserDTO.class)
@JsonDeserialize(as = ImmutableUserDTO.class)
public interface UserDTO {

    String email();

    String firstName();

    String lastName();

    String phoneNumber();

    Optional<List<RoleDTO>> roles();
}
