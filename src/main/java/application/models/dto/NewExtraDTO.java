package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;
import org.joda.money.Money;

@Value.Immutable
@JsonSerialize(as = ImmutableNewExtraDTO.class)
@JsonDeserialize(as = ImmutableNewExtraDTO.class)
public interface NewExtraDTO {

    String name();

    Money price();
}
