package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
@JsonSerialize(as = ImmutableNewCategoryDTO.class)
@JsonDeserialize(as = ImmutableNewCategoryDTO.class)
public interface NewCategoryDTO {

    String name();

    Optional<String> description();
}
