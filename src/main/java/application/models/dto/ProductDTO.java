package application.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;
import org.joda.money.Money;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Value.Immutable
@JsonInclude(JsonInclude.Include.NON_ABSENT)
@JsonSerialize(as = ImmutableProductDTO.class)
@JsonDeserialize(as = ImmutableProductDTO.class)
public interface ProductDTO {

    UUID id();

    String name();

    Optional<String> description();

    Money price();

    String thumbnail();

    Optional<CategoryDTO> category();

    Optional<List<ExtraDTO>> extras();
}
