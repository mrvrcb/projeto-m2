package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;
import org.joda.money.Money;

import java.util.UUID;

@Value.Immutable
@JsonSerialize(as = ImmutableExtraDTO.class)
@JsonDeserialize(as = ImmutableExtraDTO.class)
public interface ExtraDTO {

    UUID id();

    String name();

    Money price();
}
