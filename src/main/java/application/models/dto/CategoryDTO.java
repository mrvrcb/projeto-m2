package application.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Value.Immutable
@JsonInclude(JsonInclude.Include.NON_ABSENT)
@JsonSerialize(as = ImmutableCategoryDTO.class)
@JsonDeserialize(as = ImmutableCategoryDTO.class)
public interface CategoryDTO {

    UUID id();

    String name();

    String description();

    Optional<List<ProductDTO>> products();
}
