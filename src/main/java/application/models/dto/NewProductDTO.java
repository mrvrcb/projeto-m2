package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;
import org.joda.money.Money;

import java.util.List;
import java.util.Optional;

@Value.Immutable
@JsonSerialize(as = ImmutableNewProductDTO.class)
@JsonDeserialize(as = ImmutableNewProductDTO.class)
public interface NewProductDTO {

    String name();

    Optional<String> description();

    Money price();

    String thumbnail();

    NewCategoryDTO category();

    Optional<List<NewExtraDTO>> extras();
}
