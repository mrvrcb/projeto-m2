package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.List;
import java.util.UUID;

@Value.Immutable
@JsonSerialize(as = ImmutableOrderDTO.class)
@JsonDeserialize(as = ImmutableOrderDTO.class)
public interface OrderDTO {

    UUID userId();

    UUID productId();

    List<UUID> selectedExtras();
}
