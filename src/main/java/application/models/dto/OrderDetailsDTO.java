package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;
import org.joda.money.Money;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Value.Immutable
@JsonSerialize(as = ImmutableOrderDetailsDTO.class)
@JsonDeserialize(as = ImmutableOrderDetailsDTO.class)
public interface OrderDetailsDTO {

    UUID id();

    UserAccountDTO user();

    List<ProductDTO> products();

    List<ExtraDTO> selectedExtras();

    @Value.Derived
    default Money total() {
        return Money.total(
                Money.total(selectedExtras().stream().map(ExtraDTO::price).collect(Collectors.toList())),
                Money.total(products().stream().map(ProductDTO::price).collect(Collectors.toList()))
        );
    }
}
