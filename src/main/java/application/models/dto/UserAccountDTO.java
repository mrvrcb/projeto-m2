package application.models.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableUserAccountDTO.class)
@JsonDeserialize(as = ImmutableUserAccountDTO.class)
public interface UserAccountDTO {

    String email();

    String password();

    String firstName();

    String lastName();

    String phoneNumber();
}
