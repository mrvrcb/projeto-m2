package application.models.to;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "roles")
public class RoleTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToMany(mappedBy = "roles")
    private Collection<UserTO> users;

    @ManyToMany
    @JoinTable(
            name = "role_authorities",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id")
    )
    private Collection<AuthorityTO> authorities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<UserTO> getUsers() {
        return users;
    }

    public void setUsers(Collection<UserTO> users) {
        this.users = users;
    }

    public Collection<AuthorityTO> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<AuthorityTO> authorities) {
        this.authorities = authorities;
    }
}