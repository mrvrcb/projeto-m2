package application.models.to;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "orders")
public class OrderTO {

    @Id
    @GeneratedValue
    @Type(type = "pg-uuid")
    private UUID id;

    @MapsId
    @OneToOne
    @JoinColumn(name = "user_id")
    private UserTO userTO;

    @GeneratedValue
    @Column(name = "created_at")
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    public UUID getId() {
        return id;
    }

    public UserTO getUserTO() {
        return userTO;
    }

    public void setUserTO(UserTO userTO) {
        this.userTO = userTO;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
