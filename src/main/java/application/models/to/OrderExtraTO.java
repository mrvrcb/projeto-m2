package application.models.to;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "order_extra")
@IdClass(OrderExtraPK.class)
public class OrderExtraTO {

    @Id
    @Column(name = "order_id")
    private UUID orderId;

    @Id
    @Column(name = "extra_id")
    private UUID extraId;

    public UUID getOrderId() {
        return orderId;
    }

    public void setOrderId(UUID orderId) {
        this.orderId = orderId;
    }

    public UUID getExtraId() {
        return extraId;
    }

    public void setExtraId(UUID extraId) {
        this.extraId = extraId;
    }
}
