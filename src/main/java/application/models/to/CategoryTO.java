package application.models.to;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "categories")
public class CategoryTO {

    @Id
    @GeneratedValue
    @Type(type = "pg-uuid")
    private UUID id;

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    private String description;

    @OneToMany(mappedBy = "categoryId", targetEntity = ProductTO.class)
    private Set<UUID> products;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<UUID> getProducts() {
        return products;
    }
}
