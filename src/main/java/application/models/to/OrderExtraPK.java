package application.models.to;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class OrderExtraPK implements Serializable {

    private UUID orderId;

    private UUID extraId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderExtraPK that = (OrderExtraPK) o;
        return Objects.equals(orderId, that.orderId) &&
                Objects.equals(extraId, that.extraId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, extraId);
    }
}
