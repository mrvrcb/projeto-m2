package application.models.to;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "extras")
public class ExtraTO {

    @Id
    @GeneratedValue
    @Type(type = "pg-uuid")
    private UUID id;

    @Column
    private String name;

    @Column
    private BigDecimal price;

    @Column(name = "parent_product_id")
    private UUID parentProductId;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public UUID getParentProductId() {
        return parentProductId;
    }

    public void setParentProductId(UUID parentProductId) {
        this.parentProductId = parentProductId;
    }
}
