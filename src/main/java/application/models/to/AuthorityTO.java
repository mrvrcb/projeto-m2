package application.models.to;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "authorities")
public class AuthorityTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToMany(mappedBy = "authorities")
    private Collection<RoleTO> roles;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<RoleTO> getRoles() {
        return roles;
    }

    public void setRoles(Collection<RoleTO> roles) {
        this.roles = roles;
    }
}
