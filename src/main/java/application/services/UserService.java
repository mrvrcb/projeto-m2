package application.services;

import application.converters.user.UserConverter;
import application.models.dto.UserAccountDTO;
import application.models.dto.UserDTO;
import application.models.to.UserTO;
import application.repositories.RoleRepository;
import application.repositories.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserConverter userConverter;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserConverter userConverter, UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userConverter = userConverter;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void save(UserAccountDTO userAccountDTO) {
        final UserTO user = userConverter.convert(
                userAccountDTO,
                roleRepository.findByName("USER"),
                bCryptPasswordEncoder.encode(userAccountDTO.password())
        );

        userRepository.save(user);
    }
}
