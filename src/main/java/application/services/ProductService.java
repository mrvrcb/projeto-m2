package application.services;

import application.converters.product.ProductConverter;
import application.models.dto.NewProductDTO;
import application.models.dto.ProductDTO;
import application.models.to.CategoryTO;
import application.models.to.ProductTO;
import application.repositories.CategoryRepository;
import application.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProductService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private ProductConverter productConverter;

    @Autowired
    private ExtraService extraService;

    private ProductDTO getProductDetails(ProductTO productTO) {
        return productConverter.convert(
                productTO,
                extraService.findByProductId(productTO.getId()),
                categoryService.get(productTO.getCategoryId())
        );
    }

    @CacheEvict({ "products", "categories" })
    public ProductTO create(NewProductDTO product) {
        return Optional
                .ofNullable(categoryService.create(product.category()))
                .map(CategoryTO::getId)
                .map(categoryId -> productConverter.convert(product, categoryId))
                .map(productsRepository::save)
                .orElse(null);
    }

    @Cacheable(value = "products")
    public List<ProductDTO> list() {
        return StreamSupport
                .stream(productsRepository.findAll().spliterator(), true)
                .map(productConverter::convert)
                .collect(Collectors.toList());
    }

    @CachePut(value = "categories", key = "#id", unless = "#!result.isPresent()")
    public Optional<ProductDTO> get(UUID id) {
        return productsRepository
                .findById(id)
                .map(this::getProductDetails);
    }

    @CacheEvict(value = "categories", key = "#id")
    public boolean delete(UUID id) {
        try {
            productsRepository.deleteById(id);
            return true;
        }
        catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
    }

    @CacheEvict(value = "categories", key = "#id")
    public ProductTO edit(UUID id, NewProductDTO newProductDTO) {
        return productsRepository.findById(id)
                .map(targetProduct -> productConverter.convert(targetProduct, newProductDTO))
                .map(productsRepository::save)
                .orElse(null);
    }

    public List<ProductDTO> findAllById(List<UUID> ids) {
        return StreamSupport
                .stream(productsRepository.findAllById(ids).spliterator(), false)
                .map(productConverter::convert)
                .collect(Collectors.toList());
    }

    public List<ProductDTO> findAllByCategoryId(UUID categoryId) {
        return productsRepository
                .findByCategoryId(categoryId)
                .stream()
                .map(productConverter::convert)
                .collect(Collectors.toList());
    }
}
