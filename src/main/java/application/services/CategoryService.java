package application.services;

import application.converters.category.CategoryConverter;
import application.models.dto.CategoryDTO;
import application.models.dto.NewCategoryDTO;
import application.models.to.CategoryTO;
import application.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryConverter categoryConverter;

    @Autowired
    private ProductService productService;

    private CategoryDTO getCategoryDetails(CategoryTO categoryTO) {
        return categoryConverter.convert(
                categoryTO,
                productService.findAllByCategoryId(categoryTO.getId())
        );
    }

    @CacheEvict(value = "categories", key = "#category.id")
    public CategoryTO create(NewCategoryDTO category) {
        CategoryTO newCategoryTO = categoryConverter.convert(category);
        return categoryRepository.save(newCategoryTO);
    }

    @CachePut(value = "categories", key = "#id", unless = "#!result.isPresent()")
    public Optional<CategoryDTO> get(UUID id) {
        return categoryRepository
                .findById(id)
                .map(this::getCategoryDetails);
    }

    @Cacheable(value = "categories", key = "#result.id")
    public List<CategoryDTO> list() {
        return StreamSupport
                .stream(categoryRepository.findAll().spliterator(), true)
                .map(categoryConverter::convert)
                .collect(Collectors.toList());
    }

    @CacheEvict(value = "categories", key = "#id")
    public boolean delete(UUID id) {
        try {
            categoryRepository.deleteById(id);
            return true;
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
    }

    @CacheEvict(value = "categories", key = "#id")
    public HttpStatus edit(UUID id, NewCategoryDTO newCategoryDTO) {
        Optional<CategoryTO> category = categoryRepository.findById(id);

        if (category.isPresent()) {
            CategoryTO categoryTODetails = categoryConverter.convert(category.get(), newCategoryDTO);

            categoryRepository.save(categoryTODetails);
            return HttpStatus.OK;
        }

        return HttpStatus.NOT_FOUND;
    }
}
