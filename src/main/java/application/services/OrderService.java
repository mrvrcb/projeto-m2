package application.services;

import application.converters.orders.OrderConverter;
import application.models.dto.OrderDTO;
import application.models.dto.OrderDetailsDTO;
import application.models.dto.ProductDTO;
import application.models.to.*;
import application.repositories.OrderExtraRepository;
import application.repositories.OrderProductRepository;
import application.repositories.OrderRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final OrderExtraRepository orderExtraRepository;
    private final OrderProductRepository orderProductRepository;

    private final ExtraService extraService;
    private final ProductService productService;

    private final OrderConverter orderConverter;

    public OrderService(
            OrderRepository orderRepository,
            OrderExtraRepository orderExtraRepository,
            OrderProductRepository orderProductRepository,
            OrderConverter orderConverter,
            ProductService productService,
            ExtraService extraService
    ) {
        this.orderRepository = orderRepository;
        this.orderExtraRepository = orderExtraRepository;
        this.orderProductRepository = orderProductRepository;
        this.orderConverter = orderConverter;
        this.productService = productService;
        this.extraService = extraService;
    }

    private OrderDetailsDTO getOrderDetails(OrderTO orderTO) {
        final List<ProductTO> productsByOrderId = orderProductRepository.findProductsByOrderId(orderTO.getId());
        final List<ExtraTO> extrasByOrderId = orderExtraRepository.findExtrasByExtraId(orderTO.getId());

        return orderConverter.convert(orderTO, productsByOrderId, extrasByOrderId);
    }

    @Cacheable("orders")
    public List<OrderDetailsDTO> list() {
        return StreamSupport
                .stream(orderRepository.findAll().spliterator(), false)
                .map(this::getOrderDetails)
                .collect(Collectors.toList());
    }

    @CacheEvict("orders")
    public boolean create(OrderDTO orderDTO) {
        try {
            OrderTO orderTO = orderConverter.convert(orderDTO);
            orderRepository.save(orderTO);

            final Optional<ProductDTO> targetProduct = productService.get(orderDTO.productId());
            if (targetProduct.isPresent()) {
                OrderProductTO orderProductTO = orderConverter.convertProduct(orderTO.getId(), targetProduct.get());
                orderProductRepository.save(orderProductTO);

                List<OrderExtraTO> orderExtraTO = orderConverter.convertExtras(orderTO.getId(), extraService.findAllById(orderDTO.selectedExtras()));
                orderExtraRepository.saveAll(orderExtraTO);

                return true;
            }

            return false;
        }
        catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
    }
}
