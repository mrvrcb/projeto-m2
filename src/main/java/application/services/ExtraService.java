package application.services;

import application.converters.extras.ExtraConverter;
import application.models.dto.ExtraDTO;
import application.models.dto.NewExtraDTO;
import application.models.to.ExtraTO;
import application.repositories.ExtraRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ExtraService {

    private final ExtraRepository extraRepository;
    private final ExtraConverter extraConverter;

    public ExtraService(ExtraRepository extraRepository, ExtraConverter extraConverter) {
        this.extraRepository = extraRepository;
        this.extraConverter = extraConverter;
    }

    public boolean delete(UUID id) {
        try {
            extraRepository.deleteById(id);
            return true;
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
    }

    public HttpStatus edit(UUID id, NewExtraDTO newExtraDTO) {
        Optional<ExtraTO> targetExtra = extraRepository.findById(id);

        if (targetExtra.isPresent()) {
            ExtraTO extraTO = extraConverter.convert(targetExtra.get(), newExtraDTO);
            extraRepository.save(extraTO);

            return HttpStatus.OK;
        }

        return HttpStatus.NOT_FOUND;
    }

    public List<ExtraDTO> findAllById(List<UUID> ids) {
        return StreamSupport
                .stream(extraRepository.findAllById(ids).spliterator(), false)
                .map(extraConverter::convert)
                .collect(Collectors.toList());
    }

    public Optional<ExtraDTO> findById(UUID id) {
        return extraRepository
                .findById(id)
                .map(extraConverter::convert);
    }

    public List<ExtraDTO> findByProductId(UUID productId) {
        return extraRepository.findByParentProductId(productId)
                .stream()
                .map(extraConverter::convert)
                .collect(Collectors.toList());
    }
}
