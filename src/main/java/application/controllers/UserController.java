package application.controllers;

import application.models.dto.UserAccountDTO;
import application.services.SecurityService;
import application.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;
    private SecurityService securityService;

    public UserController(UserService userService, SecurityService securityService) {
        this.userService = userService;
        this.securityService = securityService;
    }

    @PostMapping
    public ResponseEntity post(@RequestBody UserAccountDTO userAccountDTO) {
        userService.save(userAccountDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public ResponseEntity get() {
        return Optional
                .ofNullable(securityService.getAuthenticatedUser())
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.noContent().build());
    }
}
