package application.controllers;

import application.models.dto.UserLoginDTO;
import application.services.SecurityService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    private SecurityService securityService;

    public LoginController(SecurityService securityService) {
        this.securityService = securityService;
    }

    @PostMapping
    public ResponseEntity login(@RequestBody UserLoginDTO userLoginDTO) {
        securityService.autologin(userLoginDTO.email(), userLoginDTO.password());
        return ResponseEntity.ok().build();
    }
}
