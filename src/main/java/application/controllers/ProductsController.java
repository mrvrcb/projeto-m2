package application.controllers;

import application.models.dto.NewProductDTO;
import application.services.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/products")
public class ProductsController {

    private final ProductService productService;

    public ProductsController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity create(@RequestBody NewProductDTO product) {
        return Optional
                .ofNullable(productService.create(product))
                .map(ok -> ResponseEntity.noContent().build())
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping
    public ResponseEntity list() {
        return ResponseEntity
                .ok(productService.list());
    }

    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable UUID id) {
        return productService
                .get(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity delete(@PathVariable UUID id) {
        if (productService.delete(id)) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity edit(@PathVariable UUID id, @RequestBody NewProductDTO newProductDTO) {
        return Optional
                .ofNullable(productService.edit(id, newProductDTO))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }
}
