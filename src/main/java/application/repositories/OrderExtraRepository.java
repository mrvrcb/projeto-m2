package application.repositories;

import application.models.to.ExtraTO;
import application.models.to.OrderExtraTO;
import application.models.to.OrderExtraPK;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface OrderExtraRepository extends CrudRepository<OrderExtraTO, OrderExtraPK> {
    List<ExtraTO> findExtrasByExtraId(@Param("extra_id") UUID extraId);
}
