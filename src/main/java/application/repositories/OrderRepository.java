package application.repositories;

import application.models.to.OrderTO;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface OrderRepository extends CrudRepository<OrderTO, UUID> {
}
