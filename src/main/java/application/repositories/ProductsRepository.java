package application.repositories;

import application.models.to.ProductTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProductsRepository extends CrudRepository<ProductTO, UUID> {

    List<ProductTO> findByCategoryId(@Param("category_id") UUID categoryId);

}
