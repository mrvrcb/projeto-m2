package application.repositories;

import application.models.to.OrderProductTO;
import application.models.to.OrderProductPK;
import application.models.to.ProductTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderProductRepository extends CrudRepository<OrderProductTO, OrderProductPK> {
    List<ProductTO> findProductsByOrderId(@Param("order_id") UUID orderId);
}
