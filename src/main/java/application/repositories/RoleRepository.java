package application.repositories;

import application.models.to.RoleTO;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<RoleTO, Long> {
    RoleTO findByName(String name);
}
