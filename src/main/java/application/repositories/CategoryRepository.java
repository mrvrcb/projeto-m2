package application.repositories;

import application.models.to.CategoryTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface CategoryRepository extends CrudRepository<CategoryTO, UUID> {
}
