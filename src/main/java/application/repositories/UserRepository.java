package application.repositories;

import application.models.to.UserTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<UserTO, UUID> {
    UserTO findByEmail(@Param("email") String email);
}