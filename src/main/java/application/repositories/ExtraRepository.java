package application.repositories;

import application.models.to.ExtraTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExtraRepository extends CrudRepository<ExtraTO, UUID> {

    List<ExtraTO> findByParentProductId(@Param("parent_product_id") UUID productId);
}
