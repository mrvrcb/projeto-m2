package application.converters.orders;

import application.converters.user.UserConverter;
import application.converters.extras.ExtraConverter;
import application.converters.product.ProductConverter;
import application.models.dto.*;
import application.models.to.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class OrderConverter {

    private UserConverter userConverter;
    private ExtraConverter extraConverter;
    private ProductConverter productConverter;

    public OrderConverter(ProductConverter productConverter, UserConverter userConverter, ExtraConverter extraConverter) {
        this.productConverter = productConverter;
        this.userConverter = userConverter;
        this.extraConverter = extraConverter;
    }

    public List<OrderExtraTO> convertExtras(UUID orderId, List<ExtraDTO> extras) {
        return extras
                .stream()
                .map(extra -> {
                    OrderExtraTO orderProduct = new OrderExtraTO();
                    orderProduct.setOrderId(orderId);
                    orderProduct.setExtraId(extra.id());

                    return orderProduct;
                })
                .collect(Collectors.toList());
    }

    public OrderProductTO convertProduct(UUID orderId, ProductDTO product) {
        OrderProductTO orderProductTO = new OrderProductTO();
        orderProductTO.setOrderId(orderId);
        orderProductTO.setProductId(product.id());

        return orderProductTO;
    }

    public OrderTO convert(OrderDTO orderDTO) {
        OrderTO orderTO = new OrderTO();
        orderTO.setUserTO(orderTO.getUserTO());

        return orderTO;
    }

    public OrderDetailsDTO convert(OrderTO orderTO, List<ProductTO> productTOS, List<ExtraTO> extraTOES) {
        return ImmutableOrderDetailsDTO
                .builder()
                .id(orderTO.getId())
                .user(userConverter.convert(orderTO.getUserTO()))
                .products(productTOS.stream().map(productConverter::convert).collect(Collectors.toList()))
                .selectedExtras(extraTOES.stream().map(extraConverter::convert).collect(Collectors.toList()))
                .build();
    }
}
