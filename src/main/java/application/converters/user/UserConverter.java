package application.converters.user;

import application.models.dto.*;
import application.models.to.AuthorityTO;
import application.models.to.RoleTO;
import application.models.to.UserTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserConverter {

    public UserDTO convertToUserDetails(UserTO userTO) {
        return ImmutableUserDTO.builder()
                .email(userTO.getEmail())
                .firstName(userTO.getFirstName())
                .lastName(userTO.getLastName())
                .phoneNumber(userTO.getPhoneNumber())
                .roles(userTO
                        .getRoles()
                        .stream()
                        .map(role -> ImmutableRoleDTO
                                .builder()
                                .name(role.getName())
                                .authorities(
                                        role.getAuthorities()
                                                .stream()
                                                .map(AuthorityTO::getName)
                                                .collect(Collectors.toList())
                                )
                                .build()
                        )
                        .collect(Collectors.toList()))
                .build();
    }

    public UserAccountDTO convert(UserTO userTO) {
        return ImmutableUserAccountDTO.builder()
                .email(userTO.getEmail())
                .firstName(userTO.getFirstName())
                .lastName(userTO.getLastName())
                .phoneNumber(userTO.getPhoneNumber())
                .build();
    }

    public UserTO convert(UserAccountDTO userAccountDTO, RoleTO role, String encodedPassword) {
        UserTO userTO = new UserTO();

        userTO.setFirstName(userAccountDTO.firstName());
        userTO.setLastName(userAccountDTO.lastName());
        userTO.setPassword(encodedPassword);
        userTO.setEmail(userAccountDTO.email());
        userTO.setEnabled(true);
        userTO.setPhoneNumber(userAccountDTO.phoneNumber());
        userTO.setRoles(Collections.singletonList(role));

        return userTO;
    }
}
