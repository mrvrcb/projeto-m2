package application.converters.category;

import application.models.dto.CategoryDTO;
import application.models.dto.ImmutableCategoryDTO;
import application.models.dto.NewCategoryDTO;
import application.models.dto.ProductDTO;
import application.models.to.CategoryTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryConverter {

    public CategoryTO convert(CategoryTO categoryTO, NewCategoryDTO newCategoryDTO) {
        categoryTO.setName(newCategoryDTO.name());
        categoryTO.setDescription(newCategoryDTO.description().orElse(""));

        return categoryTO;
    }

    public CategoryTO convert(NewCategoryDTO newCategoryDTO) {
        CategoryTO categoryTO = new CategoryTO();

        categoryTO.setName(newCategoryDTO.name());
        categoryTO.setDescription(newCategoryDTO.description().orElse(""));

        return categoryTO;
    }

    public CategoryDTO convert(CategoryTO categoryTO) {
        return ImmutableCategoryDTO.builder()
                .id(categoryTO.getId())
                .name(categoryTO.getName())
                .description(categoryTO.getDescription())
                .build();
    }

    public CategoryDTO convert(CategoryTO categoryTO, List<ProductDTO> products) {
        return ImmutableCategoryDTO.builder()
                .id(categoryTO.getId())
                .name(categoryTO.getName())
                .description(categoryTO.getDescription())
                .products(products)
                .build();
    }
}
