package application.converters.product;

import application.models.dto.*;
import application.models.to.CategoryTO;
import application.models.to.ProductTO;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ProductConverter {

    public ProductTO convert(ProductTO productTO, NewProductDTO newProductDTO) {
        productTO.setName(newProductDTO.name());
        productTO.setDescription(newProductDTO.description().orElse(""));
        productTO.setPrice(newProductDTO.price().getAmount());
        productTO.setThumbnailURL(newProductDTO.thumbnail());
        productTO.setCategoryId(productTO.getCategoryId());

        return productTO;
    }

    public ProductTO convert(NewProductDTO newProductDTO, UUID categoryId) {
        ProductTO productTO = new ProductTO();

        productTO.setName(newProductDTO.name());
        productTO.setDescription(newProductDTO.description().orElse(""));
        productTO.setPrice(newProductDTO.price().getAmount());
        productTO.setThumbnailURL(newProductDTO.thumbnail());
        productTO.setCategoryId(categoryId);

        return productTO;
    }

    public ProductDTO convert(ProductTO productTO) {
        return ImmutableProductDTO.builder()
                .id(productTO.getId())
                .name(productTO.getName())
                .description(productTO.getDescription())
                .price(Money.of(CurrencyUnit.of("BRL"), productTO.getPrice()))
                .thumbnail(productTO.getThumbnailURL())
                .build();
    }

    public ProductDTO convert(ProductTO productTO, List<ExtraDTO> extras, Optional<CategoryDTO> category) {
        return ImmutableProductDTO.builder()
                .id(productTO.getId())
                .name(productTO.getName())
                .description(productTO.getDescription())
                .price(Money.of(CurrencyUnit.of("BRL"), productTO.getPrice()))
                .thumbnail(productTO.getThumbnailURL())
                .extras(extras)
                .category(category)
                .build();
    }
}
