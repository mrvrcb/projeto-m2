package application.converters.extras;

import application.models.dto.ExtraDTO;
import application.models.dto.ImmutableExtraDTO;
import application.models.dto.NewExtraDTO;
import application.models.to.ExtraTO;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.stereotype.Component;

@Component
public class ExtraConverter {

    public ExtraTO convert(ExtraTO extraTO, NewExtraDTO newExtraDTO) {
        extraTO.setName(newExtraDTO.name());
        extraTO.setPrice(newExtraDTO.price().getAmount());

        return extraTO;
    }

    public ExtraDTO convert(ExtraTO extraTO) {
        return ImmutableExtraDTO.builder()
                .id(extraTO.getId())
                .name(extraTO.getName())
                .price(Money.of(CurrencyUnit.of("BRL"), extraTO.getPrice()))
                .build();
    }
}
