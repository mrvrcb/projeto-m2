CREATE TABLE roles (

    id SERIAL,
    name TEXT,

    PRIMARY KEY (id)
);

CREATE TABLE authorities (

    id SERIAL,
    name TEXT,

    PRIMARY KEY (id)

);

CREATE TABLE role_authorities (

    role_id INTEGER,
    authority_id INTEGER,

    PRIMARY key (role_id, authority_id),
    CONSTRAINT FK_role_id FOREIGN KEY (role_id) REFERENCES roles (id),
    CONSTRAINT FK_permission_id FOREIGN KEY (authority_id) REFERENCES authorities (id)
);

CREATE TABLE user_role (

    role_id INTEGER,
    user_id UUID,

    PRIMARY key (role_id, user_id),
    CONSTRAINT FK_role_id FOREIGN KEY (role_id) REFERENCES roles (id),
    CONSTRAINT FK_user_id FOREIGN KEY (user_id) REFERENCES users (id)
);

INSERT INTO roles VALUES
    (DEFAULT, 'ADMIN'),
    (DEFAULT, 'USER');

INSERT INTO authorities VALUES
    (DEFAULT, 'EDIT_PRODUCTS'),
    (DEFAULT, 'VIEW_PRODUCTS');

INSERT INTO role_authorities VALUES
    (1, 1),
    (1, 2),

    (2, 2);