CREATE EXTENSION pgcrypto;

-- Create users table

CREATE TABLE users (

	id UUID DEFAULT gen_random_uuid(),

	email TEXT UNIQUE NOT NULL,
	password TEXT NOT NULL,

	first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,

	phone_number TEXT NOT NULL,
	enabled BOOLEAN NOT NULL,

	PRIMARY KEY (id)
);

-- Creates product tables

CREATE TABLE categories (

    id UUID DEFAULT gen_random_uuid(),

    name TEXT NOT NULL,
    description TEXT NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE products (

    id UUID DEFAULT gen_random_uuid(),

    name TEXT NOT NULL,
    description TEXT NOT NULL,
    thumbnail_url TEXT NULL,
    price DECIMAL(10, 2) NOT NULL,

    category_id UUID NOT NULL,

    PRIMARY KEY (id),
    CONSTRAINT FK_category_id FOREIGN KEY (category_id) REFERENCES categories (id)
);

CREATE TABLE extras (

    id UUID DEFAULT gen_random_uuid(),

    name TEXT NOT NULL,
    parent_product_id UUID NOT NULL,
    price DECIMAL(10, 2) NOT NULL,

    PRIMARY KEY (id),
    CONSTRAINT FK_parent_product_id FOREIGN KEY (parent_product_id) REFERENCES products (id)
);