-- INSERT ADMIN USER
INSERT INTO users (email, password, first_name, last_name, phone_number, enabled) VALUES ('admin', '$2a$04$mNxCZQWxC3S44lIgJx12IuXBEk2VVJI9nrdzsXNKWs6iCrk5YF9vS', 'admin', 'admin', '1234-7899', true);

-- ASSIGN ADMIN PERMISSIONS TO PROFILE
INSERT INTO user_role(role_id, user_id) SELECT 1, id FROM users WHERE email = 'admin';