CREATE TABLE orders (

    id UUID DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL,
    created_at DATE DEFAULT NOW(),

    PRIMARY KEY (id),
    CONSTRAINT FK_user_id FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE order_product (

    order_id UUID NOT NULL,
    product_id UUID NOT NULL,

    PRIMARY KEY (order_id, product_id),
    CONSTRAINT FK_order_id FOREIGN KEY (order_id) REFERENCES orders (id),
    CONSTRAINT FK_product_id FOREIGN KEY (product_id) REFERENCES products (id)
);

CREATE TABLE order_extra (

    order_id UUID NOT NULL,
    extra_id UUID NOT NULL,

    PRIMARY KEY (order_id, extra_id),
    CONSTRAINT FK_order_id FOREIGN KEY (order_id) REFERENCES orders (id),
    CONSTRAINT FK_extra_id FOREIGN KEY (extra_id) REFERENCES extras (id)
);